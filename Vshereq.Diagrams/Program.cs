﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Vshereq.Diagrams
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            var solutionFileName = args?.Length > 0
                ? args[0]
                : throw new ArgumentNullException(nameof(args));

            var solutionLoader = new SolutionLoader();
            var projects = await solutionLoader.GetSolutionProjectsAsync(solutionFileName);

            Console.WriteLine($"{projects.Count} projects found:");

            foreach(var project in projects.OrderBy(p => p.Name))
            {
                Console.WriteLine(project.Name);
            }

            Console.ReadKey();
        }
    }
}