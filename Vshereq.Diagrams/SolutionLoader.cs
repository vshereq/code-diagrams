﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Vshereq.Diagrams.Projectes;

namespace Vshereq.Diagrams
{
    public class SolutionLoader
    {
        public async Task<IList<Project>> GetSolutionProjectsAsync(string solutionFilePath)
        {
            if (string.IsNullOrWhiteSpace(solutionFilePath))
            {
                throw new ArgumentNullException("Please specify path to solution file");
            }
            var projectRegex = new Regex("Project*.*EndProject");

            if (string.IsNullOrWhiteSpace(solutionFilePath))
            {
                throw new ArgumentNullException("Please specify solution name");
            }

            if (!File.Exists(solutionFilePath))
            {
                throw new FileNotFoundException($"Unable to find file {solutionFilePath}");
            }

            var solutionText = await File.ReadAllTextAsync(solutionFilePath);

            if (string.IsNullOrWhiteSpace(solutionText))
            {
                throw new InvalidOperationException($"Can't read projects from file");
            }

            var projReg = new Regex(
                "Project\\(\"\\{[\\w-]*\\}\"\\) = \"([\\w _]*.*)\", \"(.*\\.(cs|vcx|vb)proj)\"",
                RegexOptions.Compiled);
            var matches = projReg.Matches(solutionText).Cast<Match>();

            return matches.Select(x => new Project { Name = x.Groups[1].Value }).ToList();
        }
    }
}