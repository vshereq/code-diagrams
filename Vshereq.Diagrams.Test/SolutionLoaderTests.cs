using System;
using System.Threading.Tasks;
using Xunit;

namespace Vshereq.Diagrams.Test
{
    public class SolutionLoaderTests
    {
        private readonly SolutionLoader _testLoader;

        public SolutionLoaderTests()
        {
            _testLoader = new SolutionLoader();
        }

        [Fact]
        public void GetSolutionProjectsAsync_EmptySolutionPath_ThrowsException()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => _testLoader.GetSolutionProjectsAsync(""));
        }

        //[Fact]
        //public void GetSolutionProjectsAsync_InvalidPath_ThrowsException()
        //{
        //    var unexistingFile = Path.Combine(_testFilePath, "file not exists");
        //    Assert.ThrowsAsync<FileNotFoundException>(() => _testLoader.GetSolutionProjectsAsync(""));
        //}

        //[Fact]
        //public void GetSolutionProjectsAsync_EmptyFile_ThrowsException()
        //{
        //    var emptyFile = Path.Combine(_testFilePath, "empty.sln");
        //    Assert.ThrowsAsync<InvalidOperationException>(() => _testLoader.GetSolutionProjectsAsync(emptyFile));
        //}

        // [Fact]
        // public async Task GetSolutionProjectsAsync_ValidFile_ThrowsException()
        // {
        //     var projects = await _testLoader.GetSolutionProjectsAsync(@"D:\Repos\vshereq.diagrams\Vshereq.Diagrams.sln");

        //     Assert.NotEmpty(projects);
        // }
    }
}