﻿namespace Vshereq.Diagrams.Projectes
{
    public class Project
    {
        public string Name { get; set; }

        public string Path { get; set; }
    }
}